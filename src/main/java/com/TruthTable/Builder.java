package com.TruthTable;

import com.TruthTable.сalculator.Calculator;
import com.TruthTable.domains.DataSet;
import com.TruthTable.domains.TruthTable;
import com.TruthTable.domains.TruthTableRow;
import com.TruthTable.parser.Canonizer;
import com.TruthTable.parser.Replacer;
import com.TruthTable.validator.DefaultValidator;

import java.util.*;

/**
 * Построитель таблиц истинности по заданному выражению
 */
public class Builder {

    private String expression;
    private Canonizer canonizer;
    private Calculator calculator;

    public Builder(String expression) {
        this.expression = expression;
        canonizer = new Canonizer(expression, new DefaultValidator());
        calculator = new Calculator();
    }

    /**
     * Построение таблицы истинности по выражению
     *
     * @return Таблица истинности
     * @throws Exception Исключения в случае некорректно введенного выражения
     */
    public TruthTable build() throws Exception {

        List<DataSet> dataSets = getDataSets();
        TruthTable table = new TruthTable(expression, canonizer.getVariables().size());
        expression = canonizer.canonize(expression); // перевод выражения в универсальную форму
        for (DataSet dataSet : dataSets) {
            table.add(new TruthTableRow(dataSet, calculator.calculate(Replacer.replace(expression, dataSet))));
        }

        return table;
    }

    /**
     * Получение всех кортежей значений. например от {0, 0, 0} до {1, 1, 1}
     *
     * @return Список кортежей
     */
    private List<DataSet> getDataSets() {

        List<Character> variables = canonizer.getVariables();
        List<DataSet> data = new ArrayList<>();
        int rowCount = (int) Math.pow(2, variables.size());

        for (int i = 0; i < rowCount; i++) {
            DataSet dataSet = new DataSet();
            for (int k = 0; k < variables.size(); k++) {
                dataSet.put(variables.get(k), ((i >> k) % 2) >= 1); // заполнение кортежа
            }
            data.add(dataSet);
        }
        return data;
    }

}
