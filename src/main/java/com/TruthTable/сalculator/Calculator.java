package com.TruthTable.сalculator;

import com.TruthTable.validator.DefaultValidator;
import com.TruthTable.validator.Validator;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Калькулятор канонизированного выражения
 */
public class Calculator {
    private OperationsRepo operations = new OperationsRepo(); // операции
    Validator validator = new DefaultValidator(); // валидатор

    /**
     * Вычисление выражения
     *
     * @param expression Выражение
     * @return Значение выражения
     * @throws Exception Некорректное выражение
     */
    public boolean calculate(String expression) throws Exception {
        return calculateRPN(parseToRPN(expression));
    }

    /**
     * Вычисление выражения, представленного в форме обратной польской нотации
     *
     * @param RPNExpression Выражение в форме обратной польской нотации
     * @return Значение выражения
     */
    private boolean calculateRPN(String RPNExpression) {

        Deque<Boolean> deque = new ArrayDeque<>();

        for (char c : RPNExpression.toCharArray()) {
            if (c == '0' || c == '1') {
                deque.push(c == '1');
            } else {
                boolean b = deque.pop();
                boolean a = deque.pop();
                deque.push(operations.getOperation(c).apply(a, b));
            }
        }

        return deque.pop();
    }

    /**
     * Перевод выражения в обратную польскую нотацию
     *
     * @param expression Выражение
     * @return Выражение в форме обратной польской нотации
     */
    private String parseToRPN(String expression) {

        Deque<Character> result = new ArrayDeque<>();
        Deque<Character> operators = new ArrayDeque<>();

        char[] tokens = expression.toCharArray();

        for (char c : tokens) {
            if (isVariable(c)) {
                // переменные
                result.push(c);
            } else if (validator.isOperator(c)) {
                // операторы
                if (operators.isEmpty()) {
                    operators.push(c);
                } else if (!validator.isBracket(operators.peek()) &&
                        operations.getPriority(c) <= operations.getPriority(operators.peek())) {
                    result.push(operators.pop());
                    operators.push(c);
                } else if (validator.isBracket(operators.peek()) ||
                        operations.getPriority(c) > operations.getPriority(operators.peek())) {
                    operators.push(c);
                }
            } else if (validator.isBracket(c)) {
                // скобки
                if (validator.isOpenBracket(c)) {
                    operators.push(c);
                } else if (validator.isCloseBracket(c)) {
                    while (!validator.isOpenBracket(operators.peek()))
                        result.push(operators.pop());
                    operators.pop();
                }
            }
        }

        for (char c : operators)
            result.push(c);

        StringBuilder res = new StringBuilder();
        for (char c : result)
            res.append(c);

        return res.reverse().toString();
    }

    /**
     * Проверка является ли символ переменной
     *
     * @param c Символ
     * @return Результат проверки
     */
    private boolean isVariable(char c) {
        return (c == '0' || c == '1');
    }

}
