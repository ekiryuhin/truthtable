package com.TruthTable.сalculator;

import java.util.HashMap;
import java.util.function.BinaryOperator;

/**
 * Хранилище операций
 */
public class OperationsRepo {
    private HashMap<Character, BinaryOperator<Boolean>> operations = new HashMap<>(); //операции, соответствующие символу
    private HashMap<Character, Integer> priorities = new HashMap<>();  // приоритеты операций

    public OperationsRepo() {

        // заполнение операций
        operations.put('|', (a, b) -> !(a && b));
        operations.put('+', (a, b) -> a || b);
        operations.put('&', (a, b) -> a && b);
        operations.put('^', (a, b) -> a ^ b);
        operations.put('→', (a, b) -> (!a) || b);
        operations.put('=', (a, b) -> a == b);
        operations.put('↓', (a, b) -> !a && !b);

        // заполнение приоритетов
        priorities.put('=', 1);
        priorities.put('→', 2);
        priorities.put('+', 3);
        priorities.put('^', 3);
        priorities.put('&', 4);
        priorities.put('|', 5);
        priorities.put('↓', 5);
    }

    /**
     * Получение операции по символу
     *
     * @param c Символ
     * @return Операция
     */
    public BinaryOperator<Boolean> getOperation(char c) {
        return operations.get(c);
    }

    /**
     * Получение приоритета по символу
     *
     * @param c Символ
     * @return Приоритет
     */
    public int getPriority(char c) {
        return priorities.get(c);
    }
}
