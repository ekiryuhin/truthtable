package com.TruthTable.validator;

/**
 * Валидатор, проверяющий допустимость введенных символов
 */
public class Validator {

    protected String variables; // переменные
    protected String operators; // операторы
    protected String openBrackets; // открывающие скобки
    protected String closeBrackets; // закрывающие скобки

    /**
     * Проверка на допустимость введенных символов
     *
     * @param expression Выражение
     * @return Результат проверки
     */
    public boolean validate(String expression) {
        for (char c : expression.toCharArray())
            if (isVariable(c) && isOperator(c) && isBracket(c))
                return false;
        return true;
    }

    /**
     * Проверка является ли символ переменной
     *
     * @param c Символ
     * @return Результат проверки
     */
    public boolean isVariable(char c) {
        return variables.indexOf(c) >= 0;
    }

    /**
     * Проверка является ли символ оператором
     *
     * @param c Символ
     * @return Результат проверки
     */
    public boolean isOperator(char c) {
        return operators.indexOf(c) >= 0;
    }

    /**
     * Проверка является ли символ скобкой
     *
     * @param c Символ
     * @return Результат проверки
     */
    public boolean isBracket(char c) {
        return (openBrackets.indexOf(c) >= 0) || (closeBrackets.indexOf(c) >= 0);
    }

    /**
     * Проверка является ли символ открывающей скобкой
     *
     * @param c Символ
     * @return Результат проверки
     */
    public boolean isOpenBracket(char c) {
        return openBrackets.indexOf(c) >= 0;
    }

    /**
     * Проверка является ли символ закрывающей скобкой
     *
     * @param c Символ
     * @return Результат проверки
     */
    public boolean isCloseBracket(char c) {
        return closeBrackets.indexOf(c) >= 0;
    }

    /**
     * Получение переменных
     *
     * @return Переменные
     */
    public String getVariables() {
        return variables;
    }

    /**
     * Получение операторов
     *
     * @return Операторы
     */
    public String getOperators() {
        return operators;
    }

}
