package com.TruthTable.validator;

/**
 * Стандартный валидатор выражения
 */
public class DefaultValidator extends Validator {

    public DefaultValidator() {
        operators = "+=&|^→↓";
        variables = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        openBrackets = "(";
        closeBrackets = ")";
    }
}
