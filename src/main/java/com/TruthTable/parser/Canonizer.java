package com.TruthTable.parser;

import com.TruthTable.validator.Validator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Канонизатор выражения. Преобразует выражение к универсальной для калькулятора форме.
 * <p>
 * Например:
 * (a +B)  & c|d    >>>     (A+B)&C|D
 * (!A + C) + !B    >>>     ((A|A)+C)+(B|B)
 */
public class Canonizer {

    private String expression; // выражение
    private Validator validator; // валидатор

    public Canonizer(String expression, Validator validator) {
        this.validator = validator;
        this.expression = expression;
    }

    /**
     * Канонизация выражения
     *
     * @param expression Выражение
     * @return Преобразованное выражение
     * @throws Exception Недопустимый символ
     */
    public String canonize(String expression) throws Exception {

        expression = expression.replace(" ", ""); // удаление пробелов
        expression = replaceInversion(expression); // замена инверсии на унарную бинарную операцию
        expression = expression.toUpperCase(); // перевод в верхний регистр
        if (!validator.validate(expression)) {
            throw new Exception("Найден недопустимый символ");
        }

        return expression;
    }

    /**
     * Замена инверсии на стрелку Пирса с той же переменной
     *
     * @param expression
     * @return
     */
    private String replaceInversion(String expression) {

        StringBuilder str = new StringBuilder();
        for (int i = 0; i < expression.length(); i++) {
            if (!(expression.charAt(i) != '!')) {
                if (validator.isBracket(expression.charAt(i + 1))) { // инверсия выражения в скобках
                    expression = expression.replace("!" + copyBrackets(expression, i + 1),
                            getReplaceExpression(copyBrackets(expression, i + 1)));
                } else { // инверсия одной переменной
                    expression = expression.replaceFirst("!" + expression.charAt(i + 1),
                            getReplaceExpression(String.valueOf(expression.charAt(i + 1))));
                }
            }
            str.append(expression.charAt(i));
        }
        return str.toString();
    }

    /**
     * Обертка замененной инверсии в скобки
     *
     * @param c Переменная
     * @return Выражение в скобках
     */
    private String getReplaceExpression(String c) {
        return "(" + c + "↓" + c + ")";
    }

    /**
     * Копирование выражения в скобках
     *
     * @param expression       Выражение
     * @param openBracketIndex Позиций открывающей скобки
     * @return Выражение в скобках
     */
    private String copyBrackets(String expression, int openBracketIndex) {
        return expression.substring(openBracketIndex, expression.indexOf(")", openBracketIndex) + 1);
    }


    /**
     * Получение списка уникальных переменных
     *
     * @return Переменные
     */
    public List<Character> getVariables() {
        ArrayList<Character> variables = new ArrayList<>();

        for (char c : expression.toCharArray())
            if (validator.getVariables().indexOf(c) >= 0 && !variables.contains(c))
                variables.add(c);

        variables.sort(Comparator.reverseOrder());
        return variables;
    }
}
