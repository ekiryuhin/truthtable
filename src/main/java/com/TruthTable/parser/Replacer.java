package com.TruthTable.parser;

import com.TruthTable.domains.DataSet;

/**
 * Util-класс, заменяющий переменнные в выражении на их значения из кортежей
 */
public class Replacer {

    /**
     * Замена переменных на значения из кортежа
     *
     * @param expression Выражение
     * @param dataSet    Кортеж
     * @return Преобразованнное выражение
     */
    public static String replace(String expression, DataSet dataSet) {
        for (Character c : expression.toCharArray()) {
            Boolean value = dataSet.getValue(c);
            if (value != null) {
                expression = expression.replace(c.toString(), value ? "1" : "0");
            }
        }
        return expression;
    }
}
