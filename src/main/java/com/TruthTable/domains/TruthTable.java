package com.TruthTable.domains;

import java.util.LinkedHashSet;

/**
 * Модель таблицы истинности
 */
public class TruthTable {

    private LinkedHashSet<TruthTableRow> rows; // строки таблицы
    private String expression; // исходное выражение
    private int varCount; // кол-во переменных в выражении

    public TruthTable(String expression, int varCount) {
        this.expression = expression;
        rows = new LinkedHashSet<>();
        this.varCount = varCount;
    }

    /**
     * Добавление строки в таблицу
     *
     * @param row Строка
     */
    public void add(TruthTableRow row) {
        rows.add(row);
    }

    /**
     * Получение всех строк
     *
     * @return Строки
     */
    public LinkedHashSet<TruthTableRow> getRows() {
        return rows;
    }

    /**
     * Получение выражения
     *
     * @return Выражение
     */
    public String getExpression() {
        return expression;
    }

    /**
     * Получение кол-ва переменных
     *
     * @return Кол-во переменных
     */
    public int getVarCount() {
        return varCount;
    }
}
