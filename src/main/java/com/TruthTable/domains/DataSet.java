package com.TruthTable.domains;

import java.util.HashMap;

/**
 * Модель набора переменных с соответсвующими им значениями
 * Например: {A:0, B:1, C:1}
 */
public class DataSet {
    HashMap<Character, Boolean> data;

    public DataSet() {
        data = new HashMap<>();
    }

    /**
     * Получение всех перменных со значениями
     *
     * @return Переменные со значениями
     */
    public HashMap<Character, Boolean> getData() {
        return data;
    }

    /**
     * Добавление новой перменной
     *
     * @param var   Переменная
     * @param value Значение
     */
    public void put(Character var, Boolean value) {
        data.put(var, value);
    }

    /**
     * Получение значения перменной
     *
     * @param var Переменная
     * @return Значение
     */
    public Boolean getValue(Character var) {
        return data.get(var);
    }
}
