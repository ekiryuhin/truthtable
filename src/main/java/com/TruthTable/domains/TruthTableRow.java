package com.TruthTable.domains;

/**
 * Модель строки таблицы
 */
public class TruthTableRow {

    private DataSet dataSet; // кортеж значений дя данной строки
    private boolean value; // результат значения выражения для данного кортежа

    public TruthTableRow(DataSet dataSet, boolean value) {
        this.dataSet = dataSet;
        this.value = value;
    }

    /**
     * Получение кортежа
     *
     * @return Кортеж
     */
    public DataSet getDataSet() {
        return dataSet;
    }

    /**
     * Получение результата
     *
     * @return Результат
     */
    public boolean value() {
        return value;
    }
}
