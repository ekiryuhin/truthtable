import com.TruthTable.Builder;
import com.TruthTable.domains.TruthTable;
import com.TruthTable.domains.TruthTableRow;
import com.TruthTable.validator.DefaultValidator;
import javafx.fxml.FXML;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class Controller {

    @FXML
    TextField expField;
    @FXML
    AnchorPane pane;

    ArrayList<Node> elements = new ArrayList<>();

    public void build() {
        clearTable();
        if (!isEmpty(expField.getText())) {
            Builder builder = new Builder(expField.getText());
            try {
                TruthTable truthTable = builder.build();
                drawTable(expField.getText(), truthTable);
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.show();
                e.printStackTrace();
            }
        }
    }

    private void drawTable(String expression, TruthTable table) {
        ArrayList<Character> vars = getVars(expression);
        int HEADER_TOP_ANCHOR = 30;
        int HEADER_LEFT_ANCHOR = 50;

        for (char c : vars) {
            Label label = new Label(c + "");
            AnchorPane.setLeftAnchor(label, (double) HEADER_LEFT_ANCHOR);
            AnchorPane.setTopAnchor(label, (double) HEADER_TOP_ANCHOR);
            HEADER_LEFT_ANCHOR += 30;
            pane.getChildren().add(label);
            elements.add(label);
        }

        Label label = new Label( "RESULT");
        AnchorPane.setLeftAnchor(label, (double) HEADER_LEFT_ANCHOR);
        AnchorPane.setTopAnchor(label, (double) HEADER_TOP_ANCHOR);
        pane.getChildren().add(label);
        elements.add(label);

        for (TruthTableRow row : table.getRows()) {
            HEADER_TOP_ANCHOR += 30;
            HEADER_LEFT_ANCHOR = 50;
            for (HashMap.Entry<Character, Boolean> var : row.getDataSet().getData().entrySet()) {
                Label valueLabel = new Label(var.getValue() ? "1" : "0");
                AnchorPane.setLeftAnchor(valueLabel, (double) HEADER_LEFT_ANCHOR);
                AnchorPane.setTopAnchor(valueLabel, (double) HEADER_TOP_ANCHOR);
                HEADER_LEFT_ANCHOR += 30;
                pane.getChildren().add(valueLabel);
                elements.add(valueLabel);
            }

            Label valueLabel = new Label(row.value() ? "1" : "0");
            AnchorPane.setLeftAnchor(valueLabel, (double) HEADER_LEFT_ANCHOR);
            AnchorPane.setTopAnchor(valueLabel, (double) HEADER_TOP_ANCHOR);
            pane.getChildren().add(valueLabel);
            elements.add(valueLabel);
        }


    }

    private ArrayList<Character> getVars(String expression) {
        ArrayList<Character> vars = new ArrayList<>();
        DefaultValidator validator = new DefaultValidator();
        for (char c : expression.toCharArray())
            if (validator.isVariable(c))
                if (!vars.contains(c)) vars.add(c);
        vars.sort(Comparator.naturalOrder());
        return vars;
    }

    private boolean isEmpty(String string) {
        return string == null || string.replace(" ", "") == "";
    }

    private void clearTable() {
        for (Node node : elements)
            pane.getChildren().remove(node);
    }
}
